# Ecommerce Wordpress - Divi Theme

Features:

* Landing Page
* Shop Page
* Filter Price
* Upselling, Cross-Selling, Grouped-Selling
* Integrated with MailChimp for Subscription Coupons
* Woocommerce
* Divi Theme
* Contact Page
* FAQ Page
* Featured Product
* Top Rated Product

## Local Server

* Laragon
* PHP 7
* PHPMyadmin
* MySql

## Note:
I included the database named "ecommerce-wp.sql" and below is the credentials account for login to dashboard.

* Username: admin
* Password: admin

I'm not hosted in CPanel because I want to minimize the budged