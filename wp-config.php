<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'belajar_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'a(j6&)Xn]JwxBj[@Qj*X.qX$AX 0Bk6lW= cU4?!PHH3tVit1T_jmjz~[;oo)E5m');
define('SECURE_AUTH_KEY',  '|vai>f:=tB]tAh9b`P6[FkkfP!a4*|o]g~3}_<5ZL]ZId9478]YW{ YZE4*x?.lJ');
define('LOGGED_IN_KEY',    '#Dd7#OB 9Wr;<:d.MmC]J]$rZu^/t~21A.bHHnejuN]*t1N}{QxNkhHkkb/5Oh:o');
define('NONCE_KEY',        'yxXM44#<.vyO3~E&bs71!c3Etlch=}kTnNd:KhB~.h#.js2McTrPhKJ/[=+o@m7g');
define('AUTH_SALT',        'gY^js_%xa&xfU<7~>n7Pa%0/|M)@)=VJ;|! g(|9ZeeD?61R.mc^<?${B~NIZM=]');
define('SECURE_AUTH_SALT', 'FAZu/z{T[1z9u!K)^KJXIDVW Z=+WdIp1N57kbw&|z,!@MZ,*aq/M|i6B yWOJGM');
define('LOGGED_IN_SALT',   ');%v@%4#|1_U_a ((:%=~_0!S]w8Wd9[],:7L7ca2dN@;Uff,iMxh?7Eb/cJH8$t');
define('NONCE_SALT',       'Dy2F4bl&y{^ /gh1[uFb9z{._^!yn,}}&xltJV[P%/Nqg|2$AC&$bXI.iNL}x&Wm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
